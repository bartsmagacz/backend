import { Router } from 'express'
import { TodoItem } from '../models/todoItem'
import { stateChecker } from '../utils'
import { ERRORS } from '../consts'

export const todoRoutes = () => {
  const router = Router()

  router.get('/', (req, res) => {
    TodoItem.find((err, todoItems) => {
      if (err) {
        res.status(500).send({ message: err.message })
        throw err
      } else {
        res.json(todoItems)
      }
    })
  })

  router.post('/', (req, res) => {
    const todoItem = new TodoItem({
      title: req.body.title,
      description: req.body.description
    })

    todoItem
      .save()
      .then(data => {
        res.status(201).send(data)
      })
      .catch(err => {
        res.status(500).send({ message: err.message })
      })
  })
  // TODO: add try catch
  router.put('/', async (req, res) => {
    let todoItemToChange = {}
    await TodoItem.findById(req.body._id, async (err, todoItem) => {
      if (err) {
        res.status(500).send({ message: err.message })
        throw err
      } else {
        todoItemToChange = await todoItem
      }
    })

    if (stateChecker(todoItemToChange.state, req.body.state)) {
      TodoItem.findByIdAndUpdate(req.body._id, { state: req.body.state },
        { new: true, useFindAndModify: true },
        async (err, updatedItem) => {
          if (err) {
            res.status(500).send({ message: err.message })
            throw err
          } else {
            await res.status(201).json(updatedItem)
          }
        })
    } else {
      res.status(400).send({ message: ERRORS.CANT_CHANGE_STATUS })
    }
  })

  router.delete('/', async (req, res) => {
    TodoItem.remove({ _id: req.body._id },
      async (err) => {
        if (err) {
          res.status(500).send({ message: err.message })
          throw err
        } else {
          await res.status(201).send({ _id: req.body._id })
        }
      })
  })

  return router
}
