import { STATES } from '../consts'

const randomString = (subStringValue = 7) => {
  return Math.random().toString(36).substring(subStringValue)
}

export const createRandomTodoItem = () => (
  {
    title: randomString(),
    description: randomString(2)
  }
)

export const stateChecker = (currentState, stateToChange) => {
  if (currentState === STATES.CLOSED) {
    return false
  } else if (currentState === STATES.PENDING && stateToChange === STATES.CLOSED) {
    return true
  } else if (currentState === STATES.OPEN) {
    return true
  }
}
