import request from 'supertest'
import mongoose from 'mongoose'

import { TodoItem } from '../models/todoItem'
import { createRandomTodoItem } from '../utils'
import app from '../index'

import config from '../config.json'
import { ERRORS, STATES } from '../consts'

const todoApiUrl = `/${config.apiVersion}/todo`

let beforeAllTodoItem
let beforeAllTodoItemPending
let beforeAllTodoItemClosed

beforeAll(async (done) => {
  const randomItem = createRandomTodoItem()
  const randomItemPending = createRandomTodoItem()
  const randomItemClosed = createRandomTodoItem()
  const todoItem = new TodoItem(randomItem)
  await todoItem.save().then(data => {
    beforeAllTodoItem = data
  })
  const todoItemPending = new TodoItem({
    ...(randomItemPending),
    state: 'pending'
  })
  await todoItemPending.save().then(data => {
    beforeAllTodoItemPending = data
  })
  const todoItemClosed = new TodoItem({
    ...randomItemClosed,
    state: 'closed'
  })
  await todoItemClosed.save().then(data => {
    beforeAllTodoItemClosed = data
  })
  done()
})

afterAll(async (done) => {
  await TodoItem.remove({})
  await mongoose.disconnect()
  done()
})

describe('todo Endpoint', () => {
  it('getAll', async (done) => {
    const res = await request(app)
      .get(todoApiUrl)
    expect(res.statusCode).toEqual(200)
    expect(typeof res.body).toBe('object')
    done()
  })

  it('post todo Item', async (done) => {
    const randomTodoItem = createRandomTodoItem()
    const res = await request(app)
      .post(todoApiUrl)
      .send(randomTodoItem)

    expect(res.statusCode).toEqual(201)
    expect(res.body).toHaveProperty('title', randomTodoItem.title)
    expect(res.body).toHaveProperty('description', randomTodoItem.description)
    expect(res.body).toHaveProperty('state', 'open')
    expect(res.body).toHaveProperty('_id')
    done()
  })

  it('Update status of open task (open -> pending)', async (done) => {
    const res = await request(app)
      .put(todoApiUrl)
      .send({
        _id: beforeAllTodoItem._id,
        state: 'pending'
      })
    expect(res.statusCode).toEqual(201)
    expect(res.body.state).toEqual(STATES.PENDING)
    done()
  })

  it('Update status of pending task (pending -> closed)', async (done) => {
    const res = await request(app)
      .put(todoApiUrl)
      .send({
        _id: beforeAllTodoItem._id,
        state: 'closed'
      })
    expect(res.statusCode).toEqual(201)
    expect(res.body.state).toEqual(STATES.CLOSED)
    done()
  })

  it('Update status of pending task (pending -> open)', async (done) => {
    const res = await request(app)
      .put(todoApiUrl)
      .send({
        _id: beforeAllTodoItemPending._id,
        state: 'open'
      })
    expect(res.statusCode).toEqual(400)
    expect(res.body.message).toEqual(ERRORS.CANT_CHANGE_STATUS)
    done()
  })

  it('Update status of closed task (closed -> pending)', async (done) => {
    const res = await request(app)
      .put(todoApiUrl)
      .send({
        _id: beforeAllTodoItemClosed._id,
        state: 'pending'
      })
    expect(res.statusCode).toEqual(400)
    expect(res.body.message).toEqual(ERRORS.CANT_CHANGE_STATUS)
    done()
  })

  it('Update status of closed task (closed -> open)', async (done) => {
    const res = await request(app)
      .put(todoApiUrl)
      .send({
        _id: beforeAllTodoItemClosed._id,
        state: 'open'
      })
    expect(res.statusCode).toEqual(400)
    expect(res.body.message).toEqual(ERRORS.CANT_CHANGE_STATUS)
    done()
  })

  it('Remote task', async (done) => {
    const res = await request(app)
      .delete(todoApiUrl)
      .send({
        _id: beforeAllTodoItemClosed._id
      })
    expect(res.statusCode).toEqual(201)
    expect(String(res.body._id)).toEqual(String(beforeAllTodoItemClosed._id))
    done()
  })
})
