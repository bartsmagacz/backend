import express from 'express'
import cors from 'cors'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import dotenv from 'dotenv'

import { todoRoutes } from './api/todo'
import { initDB } from './db'

import config from './config.json'

dotenv.config()

const app = express()

// logger
app.use(morgan('dev'))

// 3rd party middleware
app.use(cors({
  exposedHeaders: config.corsHeaders
}))

app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json({
  limit: config.bodyLimit
}))

// I dont like this export
export const database = initDB()

app.use(`/${config.apiVersion}/todo`, todoRoutes())

if (process.env.NODE_ENV !== 'test') {
  app.listen(config.port, () => console.log(`Listening at http://localhost:${config.port}`))
}

export default app
