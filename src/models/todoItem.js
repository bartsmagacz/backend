import mongoose from 'mongoose'

export const todoItemSchema = new mongoose.Schema({
  title: String,
  description: String,
  state: {
    type: String,
    enum: ['open', 'pending', 'closed'],
    default: 'open'
  }
})

export const TodoItem = mongoose.model('todoItem', todoItemSchema)
