export const STATES = {
  OPEN: 'open',
  PENDING: 'pending',
  CLOSED: 'closed'
}

// TODO: Better messages
export const ERRORS = {
  CANT_CHANGE_STATUS: 'Status of this item can\'t be changed'
}
