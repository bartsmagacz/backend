import mongoose from 'mongoose'

export const initDB = async () => {
  await mongoose.connect(process.env.NODE_ENV !== 'test' ? process.env.MONGO_URL : process.env.MONGO_TEST_URL,
    { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
      if (err) {
        throw err
      }
    })
  const database = await mongoose.connection
  database.on('error', console.error.bind(console, 'connection error:'))
  return database
}
