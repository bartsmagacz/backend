To run project type ```npm run dev``` or ```yarn dev```

### Requirements:
- Node LTS (12.x)

### Used technologies:
- `Express`
- `Babel`
- `Mongoose`
- `ESlint with StandardJS`
- `Jest`
- `Supertest`
- ``


### Potential improvements:
- Fix local database
- Fix CI/CD (build, deploy to Heroku)


Made with a Express & ES6 REST API Boilerplate
