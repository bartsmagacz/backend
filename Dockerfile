FROM node:lts

RUN mkdir -p /serv
WORKDIR /serv
ADD . /serv

# set your port
ENV PORT 8080

# expose the port to outside world
EXPOSE 8080
EXPOSE 27017
